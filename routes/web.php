<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group([
    'prefix' => 'usuarios',
], function () {
    Route::get('/', 'UsuariosController@index')
         ->name('usuarios.usuario.index');
    Route::get('/create','UsuariosController@create')
         ->name('usuarios.usuario.create');
    Route::get('/show/{usuario}','UsuariosController@show')
         ->name('usuarios.usuario.show')->where('id', '[0-9]+');
    Route::get('/{usuario}/edit','UsuariosController@edit')
         ->name('usuarios.usuario.edit')->where('id', '[0-9]+');
    Route::post('/', 'UsuariosController@store')
         ->name('usuarios.usuario.store');
    Route::put('usuario/{usuario}', 'UsuariosController@update')
         ->name('usuarios.usuario.update')->where('id', '[0-9]+');
    Route::delete('/usuario/{usuario}','UsuariosController@destroy')
         ->name('usuarios.usuario.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'comunas',
], function () {
    Route::get('/', 'ComunasController@index')
         ->name('comunas.comuna.index');
    Route::get('/create','ComunasController@create')
         ->name('comunas.comuna.create');
    Route::get('/show/{comuna}','ComunasController@show')
         ->name('comunas.comuna.show')->where('id', '[0-9]+');
    Route::get('/{comuna}/edit','ComunasController@edit')
         ->name('comunas.comuna.edit')->where('id', '[0-9]+');
    Route::post('/', 'ComunasController@store')
         ->name('comunas.comuna.store');
    Route::put('comuna/{comuna}', 'ComunasController@update')
         ->name('comunas.comuna.update')->where('id', '[0-9]+');
    Route::delete('/comuna/{comuna}','ComunasController@destroy')
         ->name('comunas.comuna.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'cargos',
], function () {
    Route::get('/', 'CargosController@index')
         ->name('cargos.cargo.index');
    Route::get('/create','CargosController@create')
         ->name('cargos.cargo.create');
    Route::get('/show/{cargo}','CargosController@show')
         ->name('cargos.cargo.show')->where('id', '[0-9]+');
    Route::get('/{cargo}/edit','CargosController@edit')
         ->name('cargos.cargo.edit')->where('id', '[0-9]+');
    Route::post('/', 'CargosController@store')
         ->name('cargos.cargo.store');
    Route::put('cargo/{cargo}', 'CargosController@update')
         ->name('cargos.cargo.update')->where('id', '[0-9]+');
    Route::delete('/cargo/{cargo}','CargosController@destroy')
         ->name('cargos.cargo.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'ciudads',
], function () {
    Route::get('/', 'CiudadsController@index')
         ->name('ciudads.ciudad.index');
    Route::get('/create','CiudadsController@create')
         ->name('ciudads.ciudad.create');
    Route::get('/show/{ciudad}','CiudadsController@show')
         ->name('ciudads.ciudad.show')->where('id', '[0-9]+');
    Route::get('/{ciudad}/edit','CiudadsController@edit')
         ->name('ciudads.ciudad.edit')->where('id', '[0-9]+');
    Route::post('/', 'CiudadsController@store')
         ->name('ciudads.ciudad.store');
    Route::put('ciudad/{ciudad}', 'CiudadsController@update')
         ->name('ciudads.ciudad.update')->where('id', '[0-9]+');
    Route::delete('/ciudad/{ciudad}','CiudadsController@destroy')
         ->name('ciudads.ciudad.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'regions',
], function () {
    Route::get('/', 'RegionsController@index')
         ->name('regions.region.index');
    Route::get('/create','RegionsController@create')
         ->name('regions.region.create');
    Route::get('/show/{region}','RegionsController@show')
         ->name('regions.region.show')->where('id', '[0-9]+');
    Route::get('/{region}/edit','RegionsController@edit')
         ->name('regions.region.edit')->where('id', '[0-9]+');
    Route::post('/', 'RegionsController@store')
         ->name('regions.region.store');
    Route::put('region/{region}', 'RegionsController@update')
         ->name('regions.region.update')->where('id', '[0-9]+');
    Route::delete('/region/{region}','RegionsController@destroy')
         ->name('regions.region.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'comunas',
], function () {
    Route::get('/', 'ComunasController@index')
         ->name('comunas.comunas.index');
    Route::get('/create','ComunasController@create')
         ->name('comunas.comunas.create');
    Route::get('/show/{comunas}','ComunasController@show')
         ->name('comunas.comunas.show')->where('id', '[0-9]+');
    Route::get('/{comunas}/edit','ComunasController@edit')
         ->name('comunas.comunas.edit')->where('id', '[0-9]+');
    Route::post('/', 'ComunasController@store')
         ->name('comunas.comunas.store');
    Route::put('comunas/{comunas}', 'ComunasController@update')
         ->name('comunas.comunas.update')->where('id', '[0-9]+');
    Route::delete('/comunas/{comunas}','ComunasController@destroy')
         ->name('comunas.comunas.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'usuarios',
], function () {
    Route::get('/', 'UsuariosController@index')
         ->name('usuarios.usuarios.index');
    Route::get('/create','UsuariosController@create')
         ->name('usuarios.usuarios.create');
    Route::get('/show/{usuarios}','UsuariosController@show')
         ->name('usuarios.usuarios.show')->where('id', '[0-9]+');
    Route::get('/{usuarios}/edit','UsuariosController@edit')
         ->name('usuarios.usuarios.edit')->where('id', '[0-9]+');
    Route::post('/', 'UsuariosController@store')
         ->name('usuarios.usuarios.store');
    Route::put('usuarios/{usuarios}', 'UsuariosController@update')
         ->name('usuarios.usuarios.update')->where('id', '[0-9]+');
    Route::delete('/usuarios/{usuarios}','UsuariosController@destroy')
         ->name('usuarios.usuarios.destroy')->where('id', '[0-9]+');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@index')->name('logout');
