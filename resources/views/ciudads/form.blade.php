
<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
    <label for="nombre" class="col-md-2 control-label">Nombre</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre" type="text" id="nombre" value="{{ old('nombre', optional($ciudad)->nombre) }}" minlength="1" placeholder="Enter nombre here...">
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('region_id') ? 'has-error' : '' }}">
    <label for="region_id" class="col-md-2 control-label">Region</label>
    <div class="col-md-10">
        <select class="form-control" id="region_id" name="region_id">
        	    <option value="" style="display: none;" {{ old('region_id', optional($ciudad)->region_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select region</option>
        	@foreach ($regions as $key => $region)
			    <option value="{{ $key }}" {{ old('region_id', optional($ciudad)->region_id) == $key ? 'selected' : '' }}>
			    	{{ $region }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

