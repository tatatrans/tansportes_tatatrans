@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Usuarios</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('usuarios.usuarios.create') }}" class="btn btn-success" title="Create New Usuarios">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        @if(count($usuariosObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Usuarios Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellidopat</th>
                            <th>Apellidomat</th>
                            <th>Direccion</th>
                            <th>Comuna</th>
                            <th>Cargo</th>
                            <th>Username</th>
                            {{-- <th>Password</th> --}}

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($usuariosObjects as $usuarios)
                        <tr>
                            <td>{{ $usuarios->nombre }}</td>
                            <td>{{ $usuarios->apellidopat }}</td>
                            <td>{{ $usuarios->apellidomat }}</td>
                            <td>{{ $usuarios->direccion }}</td>
                            <td>{{ optional($usuarios->comuna)->nombre }}</td>
                            <td>{{ optional($usuarios->cargo)->nombre }}</td>
                            <td>{{ $usuarios->username }}</td>
                            {{-- <td>{{ $usuarios->password }}</td> --}}

                            <td>

                                <form method="POST" action="{!! route('usuarios.usuarios.destroy', $usuarios->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('usuarios.usuarios.show', $usuarios->id ) }}" class="btn btn-info" title="Show Usuarios">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('usuarios.usuarios.edit', $usuarios->id ) }}" class="btn btn-primary" title="Edit Usuarios">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Usuarios" onclick="return confirm(&quot;Click Ok to delete Usuarios.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $usuariosObjects->render() !!}
        </div>

        @endif

    </div>
@endsection
