@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Usuarios' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('usuarios.usuarios.destroy', $usuarios->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('usuarios.usuarios.index') }}" class="btn btn-primary" title="Show All Usuarios">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('usuarios.usuarios.create') }}" class="btn btn-success" title="Create New Usuarios">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('usuarios.usuarios.edit', $usuarios->id ) }}" class="btn btn-primary" title="Edit Usuarios">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Usuarios" onclick="return confirm(&quot;Click Ok to delete Usuarios.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre</dt>
            <dd>{{ $usuarios->nombre }}</dd>
            <dt>Apellidopat</dt>
            <dd>{{ $usuarios->apellidopat }}</dd>
            <dt>Apellidomat</dt>
            <dd>{{ $usuarios->apellidomat }}</dd>
            <dt>Direccion</dt>
            <dd>{{ $usuarios->direccion }}</dd>
            <dt>Comuna</dt>
            <dd>{{ optional($usuarios->comuna)->nombre }}</dd>
            <dt>Cargo</dt>
            <dd>{{ optional($usuarios->cargo)->nombre }}</dd>
            <dt>Username</dt>
            <dd>{{ $usuarios->username }}</dd>
            {{-- <dt>Password</dt>
            <dd>{{ $usuarios->password }}</dd> --}}

        </dl>

    </div>
</div>

@endsection
