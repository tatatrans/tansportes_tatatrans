
<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
    <label for="nombre" class="col-md-2 control-label">Nombre</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre" type="text" id="nombre" value="{{ old('nombre', optional($usuarios)->nombre) }}" minlength="1" placeholder="Ingrese Nombre..">
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('apellidopat') ? 'has-error' : '' }}">
    <label for="apellidopat" class="col-md-2 control-label">Apellidopat</label>
    <div class="col-md-10">
        <input class="form-control" name="apellidopat" type="text" id="apellidopat" value="{{ old('apellidopat', optional($usuarios)->apellidopat) }}" minlength="1" placeholder="Ingrese Apellido Paterno...">
        {!! $errors->first('apellidopat', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('apellidomat') ? 'has-error' : '' }}">
    <label for="apellidomat" class="col-md-2 control-label">Apellidomat</label>
    <div class="col-md-10">
        <input class="form-control" name="apellidomat" type="text" id="apellidomat" value="{{ old('apellidomat', optional($usuarios)->apellidomat) }}" minlength="1" placeholder="Ingrese Apellido Materno...">
        {!! $errors->first('apellidomat', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }}">
    <label for="direccion" class="col-md-2 control-label">Direccion</label>
    <div class="col-md-10">
        <input class="form-control" name="direccion" type="text" id="direccion" value="{{ old('direccion', optional($usuarios)->direccion) }}" minlength="1" placeholder="Ingrese Direccion..">
        {!! $errors->first('direccion', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('comuna_id') ? 'has-error' : '' }}">
    <label for="comuna_id" class="col-md-2 control-label">Comuna</label>
    <div class="col-md-10">
        <select class="form-control" id="comuna_id" name="comuna_id">
        	    <option value="" style="display: none;" {{ old('comuna_id', optional($usuarios)->comuna_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select comuna</option>
        	@foreach ($comunas as $key => $comuna)
			    <option value="{{ $key }}" {{ old('comuna_id', optional($usuarios)->comuna_id) == $key ? 'selected' : '' }}>
			    	{{ $comuna }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('comuna_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('cargo_id') ? 'has-error' : '' }}">
    <label for="cargo_id" class="col-md-2 control-label">Cargo</label>
    <div class="col-md-10">
        <select class="form-control" id="cargo_id" name="cargo_id">
        	    <option value="" style="display: none;" {{ old('cargo_id', optional($usuarios)->cargo_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select cargo</option>
        	@foreach ($cargos as $key => $cargo)
			    <option value="{{ $key }}" {{ old('cargo_id', optional($usuarios)->cargo_id) == $key ? 'selected' : '' }}>
			    	{{ $cargo }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('cargo_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
    <label for="username" class="col-md-2 control-label">Username</label>
    <div class="col-md-10">
        <input class="form-control" name="username" type="text" id="username" value="{{ old('username', optional($usuarios)->username) }}" minlength="1" placeholder="Ingrese Nombre Usuario...">
        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
    <label for="password" class="col-md-2 control-label">Password</label>
    <div class="col-md-10">
        <input class="form-control" name="password" type="password" id="password" value="{{ old('password', optional($usuarios)->password) }}" placeholder="Ingrese password...">
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>

