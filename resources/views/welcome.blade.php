<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Transportes Tataprime</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                background: url("/images/TataTransv2.png");
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }


            .vertical-menu {
  width: 200px; /* Set a width if you like */
  position: left;
                right: 1x;
                top: 100px;
}

.vertical-menu a {
  background-color: #eee; /* Grey background color */
  color: black; /* Black text color */
  display: block; /* Make the links appear below each other */
  padding: 12px; /* Add some padding */
  text-decoration: none; /* Remove underline from links */
}

.vertical-menu a:hover {
  background-color: #ccc; /* Dark grey background on mouse-over */
}

.vertical-menu a.active {
  background-color: #4CAF50; /* Add a green color to the "active/current" link */
  color: white;
}
        </style>
    </head>
    <body>




            <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar">A</span>
                          <span class="icon-bar">B</span>
                          <span class="icon-bar">C</span>
                        </button>
                        {{-- <a href="{!! url('/') !!}" class="navbar-brand">{{ config('app.name', 'Cargos') }}</a> --}}
                      </div>
                      <div id="navbar" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                          <li class="active"><a href="{!! url('/') !!}">Home</a></li>
                        </ul>

                        @if (Route::has('login'))
                          <ul class="nav navbar-nav navbar-right">
                          @if (Auth::check())
                              <li><a href="{{ url('/home') }}">Home</a></li>
                              <li><a href="{{ url('/usuarios') }}">Usuarios</a></li>
                          @else
                              <li><a href="{{ url('/login') }}">Login</a></li>
                              <li><a href="{{ url('/register') }}">Register</a></li>
                          @endif
                          </ul>
                        @endif



            <div class="content">
                <div class="title m-b-md">

                </div>




            </div>
        </div>
    </body>
</html>
