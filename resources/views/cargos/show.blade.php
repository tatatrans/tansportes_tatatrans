@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Cargo' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('cargos.cargo.destroy', $cargo->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('cargos.cargo.index') }}" class="btn btn-primary" title="Show All Cargo">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('cargos.cargo.create') }}" class="btn btn-success" title="Create New Cargo">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('cargos.cargo.edit', $cargo->id ) }}" class="btn btn-primary" title="Edit Cargo">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Cargo" onclick="return confirm(&quot;Click Ok to delete Cargo.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre</dt>
            <dd>{{ $cargo->nombre }}</dd>

        </dl>

    </div>
</div>

@endsection