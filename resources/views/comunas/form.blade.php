
<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
    <label for="nombre" class="col-md-2 control-label">Nombre</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre" type="text" id="nombre" value="{{ old('nombre', optional($comunas)->nombre) }}" minlength="1" placeholder="Enter nombre here...">
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('ciudad_id') ? 'has-error' : '' }}">
    <label for="ciudad_id" class="col-md-2 control-label">Ciudad</label>
    <div class="col-md-10">
        <select class="form-control" id="ciudad_id" name="ciudad_id">
        	    <option value="" style="display: none;" {{ old('ciudad_id', optional($comunas)->ciudad_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select ciudad</option>
        	@foreach ($ciudads as $key => $ciudad)
			    <option value="{{ $key }}" {{ old('ciudad_id', optional($comunas)->ciudad_id) == $key ? 'selected' : '' }}>
			    	{{ $ciudad }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('ciudad_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

