@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Comunas' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('comunas.comunas.destroy', $comunas->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('comunas.comunas.index') }}" class="btn btn-primary" title="Show All Comunas">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('comunas.comunas.create') }}" class="btn btn-success" title="Create New Comunas">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('comunas.comunas.edit', $comunas->id ) }}" class="btn btn-primary" title="Edit Comunas">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Comunas" onclick="return confirm(&quot;Click Ok to delete Comunas.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre</dt>
            <dd>{{ $comunas->nombre }}</dd>
            <dt>Ciudad</dt>
            <dd>{{ optional($comunas->ciudad)->nombre }}</dd>

        </dl>

    </div>
</div>

@endsection