<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuarios';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nombre',
                  'apellidopat',
                  'apellidomat',
                  'direccion',
                  'comuna_id',
                  'cargo_id',
                  'username',
                  'password'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the comuna for this model.
     *
     * @return App\Models\Comuna
     */
    public function comuna()
    {
        return $this->belongsTo('App\Models\Comuna','comuna_id');
    }

    /**
     * Get the cargo for this model.
     *
     * @return App\Models\Cargo
     */
    public function cargo()
    {
        return $this->belongsTo('App\Models\Cargo','cargo_id');
    }



}
