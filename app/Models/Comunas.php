<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comunas extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comunas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nombre',
                  'ciudad_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the ciudad for this model.
     *
     * @return App\Models\Ciudad
     */
    public function ciudad()
    {
        return $this->belongsTo('App\Models\Ciudad','ciudad_id');
    }



}
