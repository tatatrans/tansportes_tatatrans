<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cargo;
use App\Models\Comuna;
use App\Models\usuarios;
use Illuminate\Http\Request;
use Exception;

class UsuariosController extends Controller
{

    /**
     * Display a listing of the usuarios.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $usuariosObjects = usuarios::with('comuna','cargo')->paginate(25);

        return view('usuarios.index', compact('usuariosObjects'));
    }

    /**
     * Show the form for creating a new usuarios.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $comunas = Comuna::pluck('nombre','id')->all();
$cargos = Cargo::pluck('nombre','id')->all();

        return view('usuarios.create', compact('comunas','cargos'));
    }

    /**
     * Store a new usuarios in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $data = $this->getData($request);

            usuarios::create($data);

            return redirect()->route('usuarios.usuarios.index')
                ->with('success_message', 'Usuarios was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified usuarios.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $usuarios = usuarios::with('comuna','cargo')->findOrFail($id);

        return view('usuarios.show', compact('usuarios'));
    }

    /**
     * Show the form for editing the specified usuarios.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $usuarios = usuarios::findOrFail($id);
        $comunas = Comuna::pluck('nombre','id')->all();
$cargos = Cargo::pluck('nombre','id')->all();

        return view('usuarios.edit', compact('usuarios','comunas','cargos'));
    }

    /**
     * Update the specified usuarios in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {

            $data = $this->getData($request);

            $usuarios = usuarios::findOrFail($id);
            $usuarios->update($data);

            return redirect()->route('usuarios.usuarios.index')
                ->with('success_message', 'Usuarios was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified usuarios from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $usuarios = usuarios::findOrFail($id);
            $usuarios->delete();

            return redirect()->route('usuarios.usuarios.index')
                ->with('success_message', 'Usuarios was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'string|min:1|nullable',
            'apellidopat' => 'string|min:1|nullable',
            'apellidomat' => 'string|min:1|nullable',
            'direccion' => 'string|min:1|nullable',
            'comuna_id' => 'nullable',
            'cargo_id' => 'nullable',
            'username' => 'string|min:1|nullable',
            'password' => 'nullable',
        ];

        $data = $request->validate($rules);


        return $data;
    }

}
