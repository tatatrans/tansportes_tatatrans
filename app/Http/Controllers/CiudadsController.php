<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Region;
use App\Models\ciudad;
use Illuminate\Http\Request;
use Exception;

class CiudadsController extends Controller
{

    /**
     * Display a listing of the ciudads.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $ciudads = ciudad::with('region')->paginate(25);

        return view('ciudads.index', compact('ciudads'));
    }

    /**
     * Show the form for creating a new ciudad.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $regions = Region::pluck('nombre','id')->all();

        return view('ciudads.create', compact('regions'));
    }

    /**
     * Store a new ciudad in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $data = $this->getData($request);

            ciudad::create($data);

            return redirect()->route('ciudads.ciudad.index')
                ->with('success_message', 'Ciudad was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified ciudad.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $ciudad = ciudad::with('region')->findOrFail($id);

        return view('ciudads.show', compact('ciudad'));
    }

    /**
     * Show the form for editing the specified ciudad.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $ciudad = ciudad::findOrFail($id);
        $regions = Region::pluck('nombre','id')->all();

        return view('ciudads.edit', compact('ciudad','regions'));
    }

    /**
     * Update the specified ciudad in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {

            $data = $this->getData($request);

            $ciudad = ciudad::findOrFail($id);
            $ciudad->update($data);

            return redirect()->route('ciudads.ciudad.index')
                ->with('success_message', 'Ciudad was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified ciudad from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $ciudad = ciudad::findOrFail($id);
            $ciudad->delete();

            return redirect()->route('ciudads.ciudad.index')
                ->with('success_message', 'Ciudad was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'string|min:1|nullable',
            'region_id' => 'nullable',
        ];

        $data = $request->validate($rules);


        return $data;
    }

}
