<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cargo;
use Illuminate\Http\Request;
use Exception;

class CargosController extends Controller
{

    /**
     * Display a listing of the cargos.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $cargos = Cargo::paginate(25);

        return view('cargos.index', compact('cargos'));
    }

    /**
     * Show the form for creating a new cargo.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {


        return view('cargos.create');
    }

    /**
     * Store a new cargo in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $data = $this->getData($request);

            Cargo::create($data);

            return redirect()->route('cargos.cargo.index')
                ->with('success_message', 'Cargo was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified cargo.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $cargo = Cargo::findOrFail($id);

        return view('cargos.show', compact('cargo'));
    }

    /**
     * Show the form for editing the specified cargo.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $cargo = Cargo::findOrFail($id);


        return view('cargos.edit', compact('cargo'));
    }

    /**
     * Update the specified cargo in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {

            $data = $this->getData($request);

            $cargo = Cargo::findOrFail($id);
            $cargo->update($data);

            return redirect()->route('cargos.cargo.index')
                ->with('success_message', 'Cargo was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified cargo from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $cargo = Cargo::findOrFail($id);
            $cargo->delete();

            return redirect()->route('cargos.cargo.index')
                ->with('success_message', 'Cargo was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'string|min:1|nullable',
        ];

        $data = $request->validate($rules);


        return $data;
    }

}
