<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Ciudad;
use App\Models\Comunas;
use Illuminate\Http\Request;
use Exception;

class ComunasController extends Controller
{

    /**
     * Display a listing of the comunas.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $comunasObjects = Comunas::with('ciudad')->paginate(25);

        return view('comunas.index', compact('comunasObjects'));
    }

    /**
     * Show the form for creating a new comunas.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $ciudads = Ciudad::pluck('nombre','id')->all();

        return view('comunas.create', compact('ciudads'));
    }

    /**
     * Store a new comunas in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $data = $this->getData($request);

            Comunas::create($data);

            return redirect()->route('comunas.comunas.index')
                ->with('success_message', 'Comunas was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified comunas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $comunas = Comunas::with('ciudad')->findOrFail($id);

        return view('comunas.show', compact('comunas'));
    }

    /**
     * Show the form for editing the specified comunas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $comunas = Comunas::findOrFail($id);
        $ciudads = Ciudad::pluck('nombre','id')->all();

        return view('comunas.edit', compact('comunas','ciudads'));
    }

    /**
     * Update the specified comunas in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {

            $data = $this->getData($request);

            $comunas = Comunas::findOrFail($id);
            $comunas->update($data);

            return redirect()->route('comunas.comunas.index')
                ->with('success_message', 'Comunas was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified comunas from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $comunas = Comunas::findOrFail($id);
            $comunas->delete();

            return redirect()->route('comunas.comunas.index')
                ->with('success_message', 'Comunas was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'string|min:1|nullable',
            'ciudad_id' => 'nullable',
        ];

        $data = $request->validate($rules);


        return $data;
    }

}
